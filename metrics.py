#!/usr/bin/env python3 
import random
import getopt
import sys
import time
import datetime
#from datetime import datetime, timedelta
from influxdb_client import InfluxDBClient, Point,WritePrecision,BucketRetentionRules, Query
from influxdb_client.client.query_api import QueryApi, QueryOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client.client.util.date_utils_pandas import PandasDateTimeHelper
from influxdb_client.client.exceptions import InfluxDBError

# Set PandasDate helper which supports nanoseconds.
import influxdb_client.client.util.date_utils as date_utils
date_utils.date_helper = PandasDateTimeHelper()
"""
User-serviceable variables below. Some of these have cl flags. 
"""
token = "9r07rZqdT1RghGeZn0pXsbdRP2qfZrAKp8dWl75UkG9n_qqIFBfsY4xNrswJ7Vl9FNvKdvYeXsmlUZqBI6X8Jw=="
# url, org and bucket to work with
url = "https://us-west-2-1.aws.cloud2.influxdata.com"
org = "tlair@netreo.com"
orgid = "d5ca760cea00887c"
debug = False

def printit(tables):
    #count = 0
    print("-> There are {} tables for this run".format(len(tables)))
    for table in tables:
        yield
        #print("-> There are {} records".format(len(table.records)))
        #for record in table.records:
        #    print(record.values)
        #    count = count + 1

def doit():

    with InfluxDBClient(url=url, token=token, org=org, debug=debug) as client:

        #query_api = client.query_api(query_options=QueryOptions(profilers=["query", "operator"]))
        query_api = client.query_api()

        #timeframes = [2,5,10,15,20,25,30,35,40]
        timeframes = [10,15,20,25,30,35,40]
        #timeframes = [60,120]
        buckets = ["matrix1","matrix2","matrix3"]
        downsamples = [5, 10, 30]

        # TBD: create/destroy bucket on the fly
        # TBD: filter(fn: (r) => r["_field"] == "ds0")
        # TBD: filter(fn: (r) => r["related_table"] == "instances")
        # etc.

        print("#bucket,timeframe,downsample,time")
        # trying to find the area that is not trivial and not too long (timeout)
        for timeframe in timeframes:
            for bucket in buckets:
                toBucketParam = bucket + "_aggregated"
                #print("Beginning write for bucket {} to {} for last {} minutes with '{}' downsample.".format(bucket,toBucketParam, timeframe,"10m"))
                stopParam = datetime.datetime.now() - datetime.timedelta(days=-2)
                #|> filter(fn: (r) => r._measurement == measureParam)
                #|> range(start: timeframeParam, stop: now())
                q = '''from(bucket: bucketParam)
                       |> range(start: timeframeParam, stop: stopParam)
                       |> filter(fn: (r) => r._field == "ds0" or r._field == "ds1") 
                       |> aggregateWindow(every: 10m, fn: mean)
                       |> to(bucket: toBucketParam, org:"d5ca760cea00887c")
                    '''
                #    "measureParam" : measureParam
                p = {
                    "bucketParam": bucket,
                    "stopParam" : stopParam,
                    "timeframeParam": datetime.datetime.now() - datetime.timedelta(minutes=timeframe),
                    "toBucketParam": toBucketParam,
                }
                
                try:
                    s1 = time.time()
                    tables = query_api.query(query=q, params=p)
                    e1 = time.time()
                    #print("Testing bucket {} for last {} minutes with '{}' measurement took {} seconds".format(bucket,timeframe, measureParam, str(e1-s1)))
                    print("{},{},{},{}".format(bucket,timeframe,"10m",str(e1-s1)))
                    printit(tables)
                    #print("============================================================================================================")
                except Exception as e:
                    print("Error with bucket {} fetching last {} minutes with '{}' downsample: {}".format(bucket,timeframe, "10m",e))
                            
                #print("Beginning write for bucket {} to {} for last {} minutes with '{}' downsample.".format(bucket,toBucketParam, timeframe,"20m"))
                q = '''from(bucket: bucketParam)
                       |> range(start: timeframeParam, stop: stopParam)
                       |> filter(fn: (r) => r._field == "ds0" or r._field == "ds1") 
                       |> aggregateWindow(every: 20m, fn: mean)
                       |> to(bucket: toBucketParam, org:"d5ca760cea00887c")
                    '''
                #    "measureParam" : measureParam
                p = {
                    "bucketParam": bucket,
                    "stopParam" : stopParam,
                    "timeframeParam": datetime.datetime.now() - datetime.timedelta(minutes=timeframe),
                    "toBucketParam": toBucketParam,
                }

                #print(p)
                try:
                    s1 = time.time()
                    tables = query_api.query(query=q, params=p)
                    e1 = time.time()
                    #print("Testing bucket {} for last {} minutes with '{}'downsample took {} seconds".format(bucket,timeframe,"20m", str(e1-s1)))
                    print("{},{},{},{}".format(bucket,timeframe,"20m",str(e1-s1)))
                    printit(tables)
                    #print("============================================================================================================")
                except Exception as e:
                    print("Error with bucket {} fetching last {} minutes with '{}' downsample: {}".format(bucket,timeframe, "20m",e))
           
                #print("Beginning write for bucket {} to {} for last {} minutes with '{}' downsample.".format(bucket,toBucketParam, timeframe,"30m"))
                q = '''from(bucket: bucketParam)
                       |> range(start: timeframeParam, stop: stopParam)
                       |> filter(fn: (r) => r._field == "ds0" or r._field == "ds1") 
                       |> aggregateWindow(every: 30m, fn: mean)
                       |> to(bucket: toBucketParam, org:"d5ca760cea00887c")
                    '''
                #    "measureParam" : measureParam
                p = {
                    "bucketParam": bucket,
                    "stopParam" : stopParam,
                    "timeframeParam": datetime.datetime.now() - datetime.timedelta(minutes=timeframe),
                    "toBucketParam": toBucketParam,
                }
                try:
                   s1 = time.time()
                   tables = query_api.query(query=q, params=p)
                   e1 = time.time()
                   #print("Testing bucket {} for last {} minutes with '{}'downsample took {} seconds".format(bucket,timeframe,"30m", str(e1-s1)))
                   print("{},{},{},{}".format(bucket,timeframe,"30m",str(e1-s1)))
                   printit(tables)
                   #print("============================================================================================================")
                except Exception as e:
                   print("Error with bucket {} fetching last {} minutes with '{}' downsample: {}".format(bucket,timeframe, "30m",e))

doit()
