#!/usr/bin/env python3 
import random
import getopt
import sys
import time
from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client.client.util.date_utils_pandas import PandasDateTimeHelper
from influxdb_client.client.exceptions import InfluxDBError

# Set PandasDate helper which supports nanoseconds.
import influxdb_client.client.util.date_utils as date_utils
date_utils.date_helper = PandasDateTimeHelper()
"""
User-serviceable variables below. Some of these have cl flags. 
"""
# If this is set, then measurement is related_table + related_table_id
distinct_measurement = False
# if this is set, the number of related_table_id is limited 
_modulo = None
# measurement label for influxdb
measurement = "perf_data"
# how many iterations for the script
loops = 2
# max for generated ds0/ds1 values
max_metric = 1000
# time between records before blurring
increment = 300
# beginning start
#start = 1635447185
start = int(time.time())
# change this to False if you want regular steps in the output
do_blur = True
# how much to blur
max_blur = 10
# You can generate an API token from the "API Tokens Tab" in the UI
token = "9r07rZqdT1RghGeZn0pXsbdRP2qfZrAKp8dWl75UkG9n_qqIFBfsY4xNrswJ7Vl9FNvKdvYeXsmlUZqBI6X8Jw=="
# url, org and bucket to work with
url = "https://us-west-2-1.aws.cloud2.influxdata.com"
org = "tlair@netreo.com"
bucket = "databucket"

"""
tweak the timestamp
"""
def blur(start):
    rand = random.randint(1,max_blur)
    if rand % 2 == 0:
        return start + rand
    else:
        return start - rand

def gen_related(rt, rtid):
    return rt+str(rtid)

class BatchingCallback(object):
    def success(self, conf: (str, str, str), data: str):
        """Successfully writen batch."""
        print(f"Written batch: {conf}, data: {data}")

    def error(self, conf: (str, str, str), data: str, exception: InfluxDBError):
        """Unsuccessfully writen batch."""
        print(f"Cannot write batch: {conf}, data: {data} due: {exception}")

    def retry(self, conf: (str, str, str), data: str, exception: InfluxDBError):
        """Retryable error."""
        print(f"Retryable error occurs for batch: {conf}, data: {data} retry: {exception}")

def doit():
    ts = start
    points = []
    global measurement
    
    for i in range(0, loops):

        # max_metric is set via -m 
        ds0 = random.randint(1,max_metric)
        ds1 = random.randint(1,max_metric)

        # blur set via -u
        if do_blur == True:
            ts = blur(ts)
        
        dt = datetime.fromtimestamp(ts)
        
        if _modulo != None:
            related = i % _modulo                
        else:
            related = i

        # For each i insert an "instances" and a "leaf" record

        ds0 = random.randint(1,max_metric)
        ds1 = random.randint(1,max_metric)

        if distinct_measurement == True:
            measurement = gen_related("instances",related)

        _instances = Point(measurement).tag("related_table","instances")\
                .tag("related_table_id",related).field("ds0",float(ds0))\
                .field("ds1", float(ds1)).time(dt).to_line_protocol()
        points.append(_instances)

        ds0 = random.randint(1,max_metric)
        ds1 = random.randint(1,max_metric)
        
        if distinct_measurement == True:
            measurement = gen_related("leaf",related)
        
        _leaf = Point(measurement).tag("related_table","leaf")\
                .tag("related_table_id",related).field("ds0",float(ds0))\
                .field("ds1", float(ds1)).time(dt).to_line_protocol()
        points.append(_leaf)

        ts += increment

    callback = BatchingCallback()
    with InfluxDBClient(url=url, token=token, org=org) as client:
        """
        Use batching API
        """
        with client.write_api(success_callback=callback.success,
                              error_callback=callback.error,
                              retry_callback=callback.retry) as write_api:
            write_api.write(bucket=bucket, record=points)
            print()
            print("Wait to finishing ingesting...")
            print()

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:i:b:p:m:l:u:x:e")

    except getopt.GetoptError as err:
        print(err)  
        sys.exit(1)

    for o, a in opts:
        if o == "-t":
            start = int(a)
        elif o == "-i":
            increment = int(a)
        elif o == "-u":
            max_blur = int(a)
        elif o == "-b":
            bucket = a
        elif o == "-p":
            measurement = a
        elif o == "-m":
            max_metric = int(a)
        elif o == "-l":
            loops = int(a)
        elif o == "-x":
            _modulo = int(a)
        elif o == "-e":
            distinct_measurement = True

    doit()
