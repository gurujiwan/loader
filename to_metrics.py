#!/usr/bin/env python3 
import random
import getopt
import sys
import time
from datetime import datetime
from influxdb_client import InfluxDBClient, Point,WritePrecision,BucketRetentionRules, Query
from influxdb_client.client.query_api import QueryApi, QueryOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client.client.util.date_utils_pandas import PandasDateTimeHelper
from influxdb_client.client.exceptions import InfluxDBError

# Set PandasDate helper which supports nanoseconds.
import influxdb_client.client.util.date_utils as date_utils
date_utils.date_helper = PandasDateTimeHelper()
"""
User-serviceable variables below. Some of these have cl flags. 
"""
token = "9r07rZqdT1RghGeZn0pXsbdRP2qfZrAKp8dWl75UkG9n_qqIFBfsY4xNrswJ7Vl9FNvKdvYeXsmlUZqBI6X8Jw=="
# url, org and bucket to work with
url = "https://us-west-2-1.aws.cloud2.influxdata.com"
org = "tlair@netreo.com"

def doit():

    with InfluxDBClient(url=url, token=token, org=org, debug=True) as client:

        q = '''from(bucket: stringParam)
                |> range(start: -5d, stop: now())
              |> filter(fn: (r) => r._measurement == "perf_data")
               |> aggregateWindow(every: 30m, fn: mean)
               |> set(key: "_measurement",value: "perf_data")
                |> to(bucket: "generated_bucket1_aggregated", org:"d5ca760cea00887c")
            '''
        p = {
            "stringParam": "generated_data1",
        }

        query_api = client.query_api(query_options=QueryOptions(profilers=["query", "operator"]))
        query_api.query(query=q, params=p)
        #query_api = client.query_api()
 
        try:
            s1 = time.time()
            tables = query_api.query("""
            from(bucket: "generated_data1")
                |>range(start: -1y)
                |> filter(fn: (r) => r._measurement == "perf_data")
                |> aggregateWindow(every: 30m, fn: mean)
                |> set(key: "_measurement",value: "perf_data")
                |> to(bucket: "generated_bucket1_aggregated", org:"d5ca760cea00887c")
            """)
            e1 = time.time()
            print("Single measurement, 1yr, all tags takes {}".format(str(e1 - s1)))
        except:
            print("error doing single measurement,1y, all tags")
 
        try:
            s = time.time()
            tables = query_api.query("""
            from(bucket: "generated_data2")
                |>range(start: -1y)
                |> aggregateWindow(every: 30m, fn: mean)
                |> to(bucket: "generated_bucket2_aggregated", org:"d5ca760cea00887c")
            """)
            e = time.time()
            print("Multi measurement, 1yr, all tags takes {}".format(str(e - s)))
        except:
            print("error doing multi measurement,1y,all tags")
 
        try:
            s = time.time()
            tables = query_api.query("""
            from(bucket: "generated_data1")
                |>range(start: -3mo, stop: -2mo) 
                |> filter(fn: (r) => r._field == "ds0")
                |> aggregateWindow(every: 30m, fn: mean) 
                |> to(bucket: "generated_bucket1_aggregated", org:"d5ca760cea00887c")
            """)
            e = time.time()
            print("Single measurement, 1 month, single tag takes {}".format(str(e - s)))
        except:
            print("error doing single measurement,1 month, single tag")
 
        try:
            s = time.time()
            tables = query_api.query("""
            from(bucket: "generated_data2")
                |>range(start: -3mo, stop: -2mo) 
                |> filter(fn: (r) => r._field == "ds0")
                |> aggregateWindow(every: 30m, fn: mean) 
                |> to(bucket: "generated_bucket2_aggregated", org:"d5ca760cea00887c")
            """)
            e = time.time()
            print("Multi measurement, 1 month, single tag takes {}".format(str(e - s)))
        except:
            print("error doing multi measurement, 1 month,single tag")
 
        try:
            s = time.time()
            tables = query_api.query("""
            from(bucket: "generated_data2")
                |>range(start: -3mo, stop: -2mo) 
                |> filter(fn: (r) => r._measurement == "instances1234")
                |> aggregateWindow(every: 30m, fn: mean) 
                |> to(bucket: "generated_bucket2_aggregated", org:"d5ca760cea00887c")
            """)
            e = time.time()
            print("Multi measurement, 1 month, filtering on tag takes {}".format(str(e - s)))
        except:
            print("error doing multi measurement, 1 month, filtering on tag")
 
        #for table in tables:
        #    print(table)
        #    for record in table.records:
        #        print(record.values)

if __name__ == "__main__":
    doit()