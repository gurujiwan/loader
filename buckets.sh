# create, insert, delete buckets -- WIP

INFLUX_TOKEN="9r07rZqdT1RghGeZn0pXsbdRP2qfZrAKp8dWl75UkG9n_qqIFBfsY4xNrswJ7Vl9FNvKdvYeXsmlUZqBI6X8Jw=="
INFLUX_ORG_ID="d5ca760cea00887c"
INFLUX_URL="us-west-2-1.aws.cloud2.influxdata.com"

for s in $(seq 1 5); do
  THIS_BUCKET=leaf$s	
  curl --location --request POST "https://${INFLUX_URL}/api/v2/buckets" \
  --header 'content-type: application/json' \
  --header "authorization: Token $INFLUX_TOKEN" \
  --data-raw '{"orgID":"'$INFLUX_ORG_ID'","name":"'$THIS_BUCKET'","retentionRules":[{"everySeconds":17280000,"type":"expire"}]}'
done

# delete an existing bucket
# DELETE https://us-west-2-1.aws.cloud2.influxdata.com/api/v2/buckets/7f21f867ec36faee
# from creation output below, snarf the id
#{
#	"id": "7f21f867ec36faee",
#	"orgID": "d5ca760cea00887c",
#	"type": "user",
#	"schemaType": "implicit",
#	"name": "leaf4",
#	"retentionRules": [
#		{
#			"type": "expire",
#			"everySeconds": 17280000
#		}
#	],
#	"createdAt": "2021-12-09T20:32:31.383705897Z",
#	"updatedAt": "2021-12-09T20:32:31.383706014Z",
#	"links": {
#		"labels": "/api/v2/buckets/7f21f867ec36faee/labels",
#		"members": "/api/v2/buckets/7f21f867ec36faee/members",
#		"org": "/api/v2/orgs/d5ca760cea00887c",
#		"owners": "/api/v2/buckets/7f21f867ec36faee/owners",
#		"self": "/api/v2/buckets/7f21f867ec36faee",
#		"write": "/api/v2/write?org=d5ca760cea00887c\u0026bucket=7f21f867ec36faee"
#	},
#	"labels": []
#}
